package application;

import java.util.ArrayList;

import entities.Cliente;

public class ListaDeClientes {

	public static void main(String[] args) {
		
		ArrayList<Cliente> clientes = new ArrayList();
		
		Cliente cliente = new Cliente("Maria Ant�nia", "Rua do Matadouro", "32146589");

		Cliente cliente1 = new Cliente("Z� Ant�nio", "Rua do Escorrega", "33332146598");

		Cliente cliente2 = new Cliente("Z� Maria", "Rua S�o Paulo", "329995689");
		
		clientes.add(cliente);
		clientes.add(cliente1);
		clientes.add(cliente2);
		
		for (Cliente c: clientes) {
			System.out.println(c);
		}
	}

}
